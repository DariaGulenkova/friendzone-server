DROP SCHEMA IF EXISTS db;

CREATE SCHEMA db;

CREATE TABLE IF NOT EXISTS `db`.`users`
(
  `userId`         BIGINT       NOT NULL AUTO_INCREMENT,
  `name`       VARCHAR(255) NOT NULL,
  `lastname`   VARCHAR(255) NOT NULL,
  `date_birth` DATE         NULL,
  `gender`     TINYINT(1)   NULL,
  `email`      VARCHAR(255) NULL,
  `password`   VARCHAR(255) NULL,
  PRIMARY KEY (`userId`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `db`.`chats`
(
  `userId`        BIGINT       NOT NULL AUTO_INCREMENT,
  `chat_name` VARCHAR(255) NULL,
  PRIMARY KEY (`userId`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `db`.`chat_users`
(
  `userId`          BIGINT  NOT NULL AUTO_INCREMENT,
  `chat_id`     BIGINT  NOT NULL,
  `user_id`     BIGINT  NOT NULL,
  `user_status` TINYINT NOT NULL,
  INDEX `FK_CHAT_ID_CHATS_idx` (`chat_id` ASC) VISIBLE,
  PRIMARY KEY (`userId`),
  CONSTRAINT `FK_CHAT_ID_CHATS`
    FOREIGN KEY (`chat_id`)
      REFERENCES `db`.`chats` (`userId`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `FK_USER_ID_USERS`
    FOREIGN KEY (`user_id`)
      REFERENCES `db`.`users` (`userId`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `db`.`messages`
(
  `userId`            BIGINT   NOT NULL AUTO_INCREMENT,
  `reply_to_id`   BIGINT   NULL,
  `text`          TEXT     NOT NULL,
  `timestamp`     DATETIME NOT NULL,
  `chat_users_id` BIGINT   NOT NULL,
  PRIMARY KEY (`userId`),
  INDEX `FK_USER_ID_CHAT_ID_idx` (`chat_users_id` ASC) VISIBLE,
  INDEX `FK_ID_MESSAGES_idx` (`reply_to_id` ASC) VISIBLE,
  CONSTRAINT `FK_CHAT_USER_ID_CHAT_ID`
    FOREIGN KEY (`chat_users_id`)
      REFERENCES `db`.`chat_users` (`userId`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  CONSTRAINT `FK_REPLY_TO_ID_MESSAGES`
    FOREIGN KEY (`reply_to_id`)
      REFERENCES `db`.`messages` (`userId`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
) ENGINE = InnoDB;

# CREATE TABLE IF NOT EXISTS `db`.`location_history`
# (
#   `user_id`   BIGINT   NOT NULL,
#   `timestamp` DATETIME NOT NULL,
#   `latitude`  DOUBLE   NOT NULL,
#   `longtude`  DOUBLE   NOT NULL,
#   PRIMARY KEY (`timestamp`, `user_id`),
#   CONSTRAINT `FK_LOCATION_USER_ID_USERS`
#     FOREIGN KEY (`user_id`)
#       REFERENCES `db`.`users` (`userId`)
#       ON DELETE NO ACTION
#       ON UPDATE NO ACTION
# ) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS training_statuses
(
    user_id      BIGINT     NOT NULL,
    timestamp    DATETIME   NOT NULL,
    elapsed_time BIGINT,
    distance     DOUBLE,
    speed        DOUBLE,
    latitude     DOUBLE,
    longitude    DOUBLE,
    kind         TINYINT(2) NOT NULL,
    PRIMARY KEY (user_id, timestamp),
    CONSTRAINT FK_TRAINING_USER_ID_USERS
        FOREIGN KEY (user_id)
            REFERENCES users (userId)
            ON DELETE NO ACTION
            ON UPDATE NO ACTION
) ENGINE = InnoDB;