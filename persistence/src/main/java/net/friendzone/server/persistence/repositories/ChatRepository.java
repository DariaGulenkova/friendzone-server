package net.friendzone.server.persistence.repositories;

import net.friendzone.server.persistence.model.Chat;

import java.util.Set;

public interface ChatRepository extends Repository<Chat, Long> {

    Set<Chat> getByUserId(long userId);
}
