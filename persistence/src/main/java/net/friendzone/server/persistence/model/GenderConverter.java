package net.friendzone.server.persistence.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class GenderConverter implements AttributeConverter<Gender, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Gender attribute) {
        return attribute.getId();
    }

    @Override
    public Gender convertToEntityAttribute(Integer dbValue) {
        return Gender.fromId(dbValue);
    }
}
