package net.friendzone.server.persistence.repositories;

import net.friendzone.server.persistence.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends Repository<User, Long> {

    Optional<User> findByEmail(String email);

    List<User> findByEmailPattern(String pattern);
}
