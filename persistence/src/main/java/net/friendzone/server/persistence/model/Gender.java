package net.friendzone.server.persistence.model;

public enum Gender {
    MALE(1),
    FEMALE(2);

    private int id;

    Gender(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static Gender fromId(int id) {
        for (Gender gender : Gender.values()) {
            if (gender.getId() == id) {
                return gender;
            }
        }
        throw new IllegalArgumentException(String.format("id %d is not valid", id));
    }
}
