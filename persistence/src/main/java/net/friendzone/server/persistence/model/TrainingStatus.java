package net.friendzone.server.persistence.model;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "training_statuses")
public class TrainingStatus {

    @EmbeddedId
    private StatusId statusId;

    @Column(name = "elapsed_time")
    private Long trainingTimeSeconds;

    @Column(name = "distance")
    private Double distanceMeters;

    @Column(name = "speed")
    private Double speedKph;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "kind", nullable = false)
    private TrainingStatusKind statusKind;

    public TrainingStatus() {
        statusId = new StatusId();
    }

    public Long getUserId() {
        return statusId.getUserId();
    }

    public void setUserId(Long userId) {
        this.statusId.setUserId(userId);
    }

    public LocalDateTime getTimestamp() {
        return statusId.getTimestamp();
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.statusId.setTimestamp(timestamp);
    }

    public Long getTrainingTimeSeconds() {
        return trainingTimeSeconds;
    }

    public void setTrainingTimeSeconds(Long trainingTimeSeconds) {
        this.trainingTimeSeconds = trainingTimeSeconds;
    }

    public Double getDistanceMeters() {
        return distanceMeters;
    }

    public void setDistanceMeters(Double distanceMeters) {
        this.distanceMeters = distanceMeters;
    }

    public Double getSpeedKph() {
        return speedKph;
    }

    public void setSpeedKph(Double speedKph) {
        this.speedKph = speedKph;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public TrainingStatusKind getStatusKind() {
        return statusKind;
    }

    public void setStatusKind(TrainingStatusKind statusKind) {
        this.statusKind = statusKind;
    }

    @Embeddable
    public static class StatusId implements Serializable {

        @Column(name = "user_id", nullable = false)
        private Long userId;

        @Column(name = "timestamp", nullable = false)
        private LocalDateTime timestamp;


        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public LocalDateTime getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(LocalDateTime timestamp) {
            this.timestamp = timestamp;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            StatusId statusId = (StatusId) o;
            return userId.equals(statusId.userId) &&
                    timestamp.equals(statusId.timestamp);
        }

        @Override
        public int hashCode() {
            return Objects.hash(userId, timestamp);
        }
    }
}
