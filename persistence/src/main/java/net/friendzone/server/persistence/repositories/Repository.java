package net.friendzone.server.persistence.repositories;

import java.util.Optional;
import java.util.Set;

public interface Repository<T, ID> {

    T save(T entity);

    Optional<T> findById(ID id);

    Set<T> findAll();

    void delete(T entity);

    void deleteById(ID id);
}
