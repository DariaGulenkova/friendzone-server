package net.friendzone.server.persistence.model;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ChatUserStatusConverter implements AttributeConverter<ChatUserStatus, Integer> {

    @Override
    public Integer convertToDatabaseColumn(ChatUserStatus attribute) {
        return attribute.getId();
    }

    @Override
    public ChatUserStatus convertToEntityAttribute(Integer dbValue) {
        return ChatUserStatus.fromId(dbValue);
    }
}
