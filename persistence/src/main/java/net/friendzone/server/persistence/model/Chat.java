package net.friendzone.server.persistence.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "chats")
public class Chat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "chat_name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "chat_id")
    private Set<ChatUser> chatUsers;

    public Chat(long id) {
        this.id = id;
    }

    public Chat() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<ChatUser> getChatUsers() {
        return chatUsers;
    }

    public void setChatUsers(Set<ChatUser> chatUsers) {
        this.chatUsers = chatUsers;
    }
}
