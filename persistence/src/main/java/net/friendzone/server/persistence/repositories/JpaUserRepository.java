package net.friendzone.server.persistence.repositories;

import net.friendzone.server.persistence.model.User;
import net.friendzone.server.persistence.model.User_;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class JpaUserRepository implements UserRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public User save(User entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public Optional<User> findById(Long id) {
        return Optional.ofNullable(em.find(User.class, id));
    }

    @Override
    @Transactional
    public Optional<User> findByEmail(String email) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> root = query.from(User.class);
        CriteriaQuery<User> selectByEmail = query.select(root).where(cb.equal(root.get(User_.email), email));
        return em.createQuery(selectByEmail)
                .getResultStream().findFirst();
    }

    @Override
    public List<User> findByEmailPattern(String pattern) {
        TypedQuery<User> select = em.createQuery("select new User(u.id, u.name, u.lastName, u.email) from User u where u.email like lower(concat('%', :pattern, '%'))", User.class);
        select.setParameter("pattern", pattern);
        return new ArrayList<>(select.getResultList());
    }

    @Override
    public Set<User> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        CriteriaQuery<User> selectAll = query.select(query.from(User.class));
        return em.createQuery(selectAll)
                .getResultStream().collect(Collectors.toSet());
    }

    @Override
    public void delete(User entity) {
        em.remove(entity);
    }

    @Override
    public void deleteById(Long id) {
        User entity = em.find(User.class, id);
        if (entity != null) {
            em.remove(entity);
        }
    }
}
