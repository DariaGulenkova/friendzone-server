package net.friendzone.server.persistence.model;

public class ChatWithTrainingStatus {

    private Chat chat;

    private TrainingStatus status;

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public TrainingStatus getStatus() {
        return status;
    }

    public void setStatus(TrainingStatus status) {
        this.status = status;
    }
}
