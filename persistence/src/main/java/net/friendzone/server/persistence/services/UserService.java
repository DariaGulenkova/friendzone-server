package net.friendzone.server.persistence.services;

import net.friendzone.server.persistence.model.User;
import net.friendzone.server.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository repository;

    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    public Optional<User> getById(long id) {
        return repository.findById(id);
    }

    public Optional<User> getByEmail(String email) {
        return repository.findByEmail(email);
    }

    public List<User> getByEmailPattern(String pattern) {
        return repository.findByEmailPattern(pattern);
    }


}
