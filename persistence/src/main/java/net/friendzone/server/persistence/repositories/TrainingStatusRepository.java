package net.friendzone.server.persistence.repositories;


import net.friendzone.server.persistence.model.TrainingStatus;

import java.util.Optional;

public interface TrainingStatusRepository extends Repository<TrainingStatus, TrainingStatus.StatusId> {

    Optional<TrainingStatus> getLastStatusByUserId(long userId);
}
