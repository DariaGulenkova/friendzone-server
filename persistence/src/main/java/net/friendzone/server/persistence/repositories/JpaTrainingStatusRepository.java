package net.friendzone.server.persistence.repositories;

import net.friendzone.server.persistence.model.TrainingStatus;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class JpaTrainingStatusRepository implements TrainingStatusRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public TrainingStatus save(TrainingStatus entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public Optional<TrainingStatus> findById(TrainingStatus.StatusId statusId) {
        return Optional.ofNullable(em.find(TrainingStatus.class, statusId));
    }

    @Override
    public Set<TrainingStatus> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<TrainingStatus> query = cb.createQuery(TrainingStatus.class);
        CriteriaQuery<TrainingStatus> selectAll = query.select(query.from(TrainingStatus.class));
        return em.createQuery(selectAll)
                .getResultStream().collect(Collectors.toSet());
    }

    @Override
    public void delete(TrainingStatus entity) {
        em.remove(entity);
    }

    @Override
    public void deleteById(TrainingStatus.StatusId statusId) {
        TrainingStatus entity = em.find(TrainingStatus.class, statusId);
        if (entity != null) {
            em.remove(entity);
        }
    }

    @Override
    public Optional<TrainingStatus> getLastStatusByUserId(long userId) {
        Query select = em.createQuery("select s from TrainingStatus s where s.statusId.userId = :userId order by s.statusId.timestamp desc");
        select.setParameter("userId", userId);
        select.setMaxResults(1);
        select.setFirstResult(0);
        List<TrainingStatus> results = select.getResultList();
        return results.isEmpty() ? Optional.empty() : Optional.of(results.get(0));
    }
}
