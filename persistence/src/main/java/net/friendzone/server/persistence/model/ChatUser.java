package net.friendzone.server.persistence.model;

import javax.persistence.*;

@Entity
@Table(name = "chat_users")
public class ChatUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chat_id")
    private Chat chat;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "user_status")
    private ChatUserStatus userStatus;

    @Transient
    private TrainingStatus lastTrainingStatus;

    public ChatUser(Long id) {
        this.id = id;
    }

    public ChatUser() {
    }

    public Long getId() {
        return id;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ChatUserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(ChatUserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public TrainingStatus getLastTrainingStatus() {
        return lastTrainingStatus;
    }

    public void setLastTrainingStatus(TrainingStatus lastTrainingStatus) {
        this.lastTrainingStatus = lastTrainingStatus;
    }
}
