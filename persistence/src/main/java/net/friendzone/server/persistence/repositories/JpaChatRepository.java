package net.friendzone.server.persistence.repositories;

import net.friendzone.server.persistence.model.Chat;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class JpaChatRepository implements ChatRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional
    public Chat save(Chat entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public Optional<Chat> findById(Long id) {
        return Optional.ofNullable(em.find(Chat.class, id));
    }

    @Override
    public Set<Chat> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Chat> query = cb.createQuery(Chat.class);
        CriteriaQuery<Chat> selectAll = query.select(query.from(Chat.class));
        return em.createQuery(selectAll)
                .getResultStream().collect(Collectors.toSet());
    }

    @Override
    public void delete(Chat entity) {
        em.remove(entity);
    }

    @Override
    public void deleteById(Long id) {
        Chat entity = em.find(Chat.class, id);
        if (entity != null) {
            em.remove(entity);
        }
    }

    @Override
    public Set<Chat> getByUserId(long userId) {
        Query select = em.createQuery("select u.chats from User u where u.id = :userId");
        select.setParameter("userId", userId);
        return new HashSet<Chat>(select.getResultList());
    }
}
