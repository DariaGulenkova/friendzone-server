package net.friendzone.server.persistence.model;

public enum ChatUserStatus {

    ACTIVE(1),
    BLOCKED(2);

    private int id;

    ChatUserStatus(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static ChatUserStatus fromId(int id) {
        for (ChatUserStatus status : ChatUserStatus.values()) {
            if (status.getId() == id) {
                return status;
            }
        }
        throw new IllegalArgumentException(String.format("id %d is not valid", id));
    }
}
