package net.friendzone.server.persistence.services;

import net.friendzone.server.persistence.model.TrainingStatus;
import net.friendzone.server.persistence.model.TrainingStatusKind;
import net.friendzone.server.persistence.repositories.TrainingStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class TrainingService {

    @Autowired
    private TrainingStatusRepository repository;

    public void startNewTrainingFor(long userId) {
        TrainingStatus status = new TrainingStatus();
        status.setUserId(userId);
        status.setTimestamp(LocalDateTime.now());
        status.setStatusKind(TrainingStatusKind.START);
        repository.save(status);
    }

    public void stopTrainingFor(long userId) {
        TrainingStatus status = new TrainingStatus();
        status.setUserId(userId);
        status.setTimestamp(LocalDateTime.now());
        status.setStatusKind(TrainingStatusKind.END);
        repository.save(status);
    }

    public void saveStatus(TrainingStatus status) {
        status.setTimestamp(LocalDateTime.now());
        status.setStatusKind(TrainingStatusKind.UPDATE);
        repository.save(status);
    }
}
