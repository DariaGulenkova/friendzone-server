package net.friendzone.server.persistence.model;

public enum TrainingStatusKind {

    START(0),
    UPDATE(1),
    END(2);

    private int id;

    TrainingStatusKind(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static TrainingStatusKind fromId(int id) {
        for (TrainingStatusKind kind : TrainingStatusKind.values()) {
            if (kind.getId() == id) {
                return kind;
            }
        }
        throw new IllegalArgumentException(String.format("id %d is not valid", id));
    }
}
