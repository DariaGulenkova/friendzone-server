package net.friendzone.server.persistence.services;

import net.friendzone.server.persistence.model.*;
import net.friendzone.server.persistence.repositories.ChatRepository;
import net.friendzone.server.persistence.repositories.TrainingStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ChatService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private TrainingStatusRepository trainingStatusRepository;

    public Optional<Chat> getById(long id) {
        return chatRepository.findById(id);
    }

    public Set<Chat> getAll() {
        return  chatRepository.findAll();
    }

    public Set<Chat> getByUserId(long userId) {
        Set<Chat> chats = chatRepository.getByUserId(userId);
        for (Chat chat : chats) {
            for (ChatUser chatUser : chat.getChatUsers()) {
                User user = chatUser.getUser();
                if (user.getId() != userId && chatUser.getUserStatus() == ChatUserStatus.ACTIVE) {
                    Optional<TrainingStatus> lastStatus = trainingStatusRepository.getLastStatusByUserId(user.getId());
                    lastStatus.ifPresent(chatUser::setLastTrainingStatus);
                }
            }
        }
        return chats;
    }

    public Chat addChat(User toUser, User whomUser) {
        Chat chat = new Chat();
        chat.setName(whomUser.getName() + " " + whomUser.getLastName());

        ChatUser toChatUser = new ChatUser();
        toChatUser.setChat(chat);
        toChatUser.setUser(toUser);
        toChatUser.setUserStatus(ChatUserStatus.ACTIVE);

        ChatUser whomChatUser = new ChatUser();
        whomChatUser.setChat(chat);
        whomChatUser.setUser(whomUser);
        whomChatUser.setUserStatus(ChatUserStatus.ACTIVE);

        chat.setChatUsers(new HashSet<>(Arrays.asList(toChatUser, whomChatUser)));
        return chatRepository.save(chat);
    }
}
