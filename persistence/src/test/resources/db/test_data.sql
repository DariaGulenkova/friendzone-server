-- Users
INSERT INTO db.users (userId, name, lastname, date_birth, gender, email, password)
VALUES (1, 'Alpha', 'Alphovich', '1984-01-01', 0, 'alpha_alphovich@mail.ru', 'qwertyA');

INSERT INTO db.users (userId, name, lastname, date_birth, gender, email, password)
VALUES (2, 'Bravo', 'Bravovich', '1984-02-06', 0, 'bravo_bravovich@mail.ru', 'qwertyB');

INSERT INTO db.users (userId, name, lastname, date_birth, gender, email, password)
VALUES (3, 'Charlie', 'Charlovna', '1984-03-20', 1, 'charlie_charlovna@mail.ru', 'qwertyC');

INSERT INTO db.users (userId, name, lastname, date_birth, gender, email, password)
VALUES (4, 'Delta', 'Deltovich', '1984-04-23', 0, 'delta_deltovich@mail.ru', 'qwertyD');

INSERT INTO db.users (userId, name, lastname, date_birth, gender, email, password)
VALUES (5, 'Epsilon', 'Epsilonovna', '1984-10-23', 1, 'epsilon_epsilonovna@mail.ru', 'qwertyE');


-- Chats
INSERT INTO db.chats (userId, chat_name)
VALUES (1, 'A - B');

INSERT INTO db.chats (userId, chat_name)
VALUES (2, 'A - C');

INSERT INTO db.chats (userId, chat_name)
VALUES (3, 'A - D');

INSERT INTO db.chats (userId, chat_name)
VALUES (4, 'B - D');

INSERT INTO db.chats (userId, chat_name)
VALUES (5, 'B - E');

INSERT INTO db.chats (userId, chat_name)
VALUES (6, 'C - D');


-- Chat_Users
INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (1, 1, 1, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (2, 1, 2, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (3, 2, 1, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (4, 2, 3, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (5, 3, 1, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (6, 3, 4, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (7, 4, 2, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (8, 4, 4, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (9, 5, 2, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (10, 5, 5, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (11, 6, 3, 1);

INSERT INTO db.chat_users (userId, chat_id, user_id, user_status)
VALUES (12, 6, 4, 1);
