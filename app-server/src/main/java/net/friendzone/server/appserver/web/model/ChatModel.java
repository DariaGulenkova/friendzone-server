package net.friendzone.server.appserver.web.model;

public class ChatModel {

    private long id;

    private String name;

    private ChatUserModel recipient;

    public ChatModel(long id) {
        this.id = id;
    }

    protected ChatModel() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ChatUserModel getRecipient() {
        return recipient;
    }

    public void setRecipient(ChatUserModel recipient) {
        this.recipient = recipient;
    }
}
