package net.friendzone.server.appserver.web.controllers;

import net.friendzone.server.persistence.model.User;
import net.friendzone.server.persistence.security.UserDetailsWithInfo;
import net.friendzone.server.persistence.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserService userService;

    @GetMapping("/list")
    public List<User> getUsersByLoginPattern(@RequestParam(name = "pattern") String pattern) {
        if (pattern.trim().isEmpty()) {
            throw new IllegalArgumentException("pattern is empty");
        }
        List<User> users = userService.getByEmailPattern(pattern);
        UserDetailsWithInfo principal = (UserDetailsWithInfo) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return users.stream().filter(user -> principal.getUserId() != user.getId()).collect(Collectors.toList());
    }
}
