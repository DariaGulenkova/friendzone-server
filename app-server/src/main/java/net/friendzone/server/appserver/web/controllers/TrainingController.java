package net.friendzone.server.appserver.web.controllers;

import net.friendzone.server.appserver.web.model.TrainingUpdate;
import net.friendzone.server.persistence.model.TrainingStatus;
import net.friendzone.server.persistence.services.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users/{userId}/training")
public class TrainingController {

    @Autowired
    private TrainingService trainingService;

    @PostMapping("/start")
    public void startTraining(@PathVariable long userId) {
        trainingService.startNewTrainingFor(userId);
    }

    @PostMapping("/stop")
    public void stopTraining(@PathVariable long userId) {
        trainingService.stopTrainingFor(userId);
    }

    @PostMapping("/update")
    public void update(@PathVariable long userId, @RequestBody TrainingUpdate update) {
        trainingService.saveStatus(convertUpdateToStatus(update, userId));
    }

    private TrainingStatus convertUpdateToStatus(TrainingUpdate update, Long userId) {
        TrainingStatus status = new TrainingStatus();
        status.setUserId(userId);
        status.setTrainingTimeSeconds(update.getTrainingTimeSeconds());
        status.setDistanceMeters(update.getDistanceMeters());
        status.setSpeedKph(update.getLocationUpdate().getSpeedKph());
        status.setLatitude(update.getLocationUpdate().getLatitude());
        status.setLongitude(update.getLocationUpdate().getLongitude());
        return status;
    }

}
