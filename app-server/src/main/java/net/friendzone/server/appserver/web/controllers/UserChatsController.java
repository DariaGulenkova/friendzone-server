package net.friendzone.server.appserver.web.controllers;

import net.friendzone.server.appserver.web.model.ChatModel;
import net.friendzone.server.appserver.web.model.ChatUserModel;
import net.friendzone.server.appserver.web.model.TrainingStatusModel;
import net.friendzone.server.persistence.model.*;
import net.friendzone.server.persistence.services.ChatService;
import net.friendzone.server.persistence.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users/{userId}/chats")
public class UserChatsController {

    @Autowired
    private ChatService chatService;

    @Autowired
    private UserService userService;

    @GetMapping
    public Set<ChatModel> getChatsForUser(@PathVariable long userId) {
        return chatService.getByUserId(userId)
                .stream()
                .map(chat -> convertChatToModel(chat, userId))
                .collect(Collectors.toSet());
    }

    @PostMapping("/add")
    public void addUserToFriendList(@PathVariable long userId, @RequestBody long friendId) {
        User user = getUser(userId);
        User friend = getUser(friendId);
        chatService.addChat(user, friend);
    }

    private User getUser(@PathVariable long userId) {
        return userService.getById(userId).orElseThrow(() -> new IllegalArgumentException("user with id " + userId + "not exists"));
    }

    private ChatModel convertChatToModel(Chat chat, long userId) {
        ChatModel model = new ChatModel(chat.getId());
        model.setName(chat.getName());
        Set<ChatUser> chatUsers = chat.getChatUsers();
        Optional<ChatUser> recipientChatUser = chatUsers.stream().filter(chatUser -> chatUser.getUser().getId() != userId).findFirst();
        model.setRecipient(convertChatUserToModel(recipientChatUser.get()));
        return model;
    }

    private ChatUserModel convertChatUserToModel(ChatUser chatUser) {
        User user = chatUser.getUser();
        ChatUserModel model = new ChatUserModel(user.getId());
        model.setActive(chatUser.getUserStatus() == ChatUserStatus.ACTIVE);
        model.setFirstName(user.getName());
        model.setLastName(user.getLastName());
        model.setLastTrainingStatus(convertTrainingStatusToModel(chatUser.getLastTrainingStatus()));
        return model;
    }

    private TrainingStatusModel convertTrainingStatusToModel(TrainingStatus trainingStatus) {
        TrainingStatusModel model = new TrainingStatusModel();
        model.setTraining(trainingStatus != null && trainingStatus.getStatusKind() != TrainingStatusKind.END);
        if (model.isTraining()) {
            model.setElapsedTimeSeconds(trainingStatus.getTrainingTimeSeconds());
            model.setDistanceMeters(trainingStatus.getDistanceMeters());
            model.setSpeedKph(trainingStatus.getSpeedKph());
            model.setLatitude(trainingStatus.getLatitude());
            model.setLongitude(trainingStatus.getLongitude());
        }
        return model;
    }
}
