package net.friendzone.server.appserver.web.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.UUID;
import java.util.stream.Stream;

@RestController
@RequestMapping("/chatting")
public class ChattingController {

    @GetMapping(value = "/events", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> events() {
        return Flux.fromStream(Stream.generate(() -> UUID.randomUUID().toString())).delayElements(Duration.ofSeconds(5));
    }
}
