package net.friendzone.server.appserver.web.model;

public class ChatUserModel {

    private long userId;

    private String firstName;

    private String lastName;

    private boolean active;

    private TrainingStatusModel lastTrainingStatus;

    public ChatUserModel(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public TrainingStatusModel getLastTrainingStatus() {
        return lastTrainingStatus;
    }

    public void setLastTrainingStatus(TrainingStatusModel lastTrainingStatus) {
        this.lastTrainingStatus = lastTrainingStatus;
    }
}
