package net.friendzone.server.authserver;

import net.friendzone.server.persistence.security.UserDetailsWithInfo;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Collections;

public class UserIdTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        DefaultOAuth2AccessToken token = new DefaultOAuth2AccessToken(accessToken);
        token.setAdditionalInformation(Collections.singletonMap("userId", ((UserDetailsWithInfo) authentication.getPrincipal()).getUserId()));
        return token;
    }
}
