package net.friendzone.server.authserver;

import net.friendzone.server.persistence.model.User;
import net.friendzone.server.persistence.security.UserDetailsWithInfo;
import net.friendzone.server.persistence.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("JpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    public JpaUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userService.getByEmail(email).orElseThrow(() -> new UsernameNotFoundException(email));
        return new UserDetailsWithInfo(user);
    }
}
